# Paged.js book template with interface

This template contains an interface to help graphic designers layout books:
- Buttons to show/hide a baseline grid and set size and position
- Button to show/hide visual markers of margin boxes 
- On reload, the web browser scroll to the place it was before reload 
- Preview mode
- Waits for the page to be returned before printing


## Instructions

- Use any local server.
- Write your content in `index.html`
- Add your style in `style-print.css`
- If you need to register handlers, you can do it in `jshandlers.js`
- To have the interface bar at the bottom of the page, add the class `interface-bar-bottom` to the body of your index.html

## Scripts included

- Paged.js 0.4.3
- `reload-in-place.js` v1.3 by Nicolas Taffin and Sameh Chafik


## Licence

MIT License https://opensource.org/licenses/MIT
Developped by Julie Blanc ([julie-blanc.fr](https://julie-blanc.fr/en/))
